/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.entities;

import com.amdisservices.amdiswebservice.interfaces.IEntity;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrija Krüger
 */
@MappedSuperclass
public class BaseEntity implements Serializable, IEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private String name;
    
    @Getter
    @Setter
    private String description;
    
    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Calendar created;
    
    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Calendar modified;
    
    @Getter
    @Setter
    private String salesforceId;
    
    @Getter
    @Setter
    private String c4cId;

    @Override
    public void setCreated() {
        created = Calendar.getInstance();
    }

    @Override
    public void setModified() {
       modified = Calendar.getInstance();
    }
}
