/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.entities;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrija Krüger
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Product extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private double width;
    
    @Getter
    @Setter
    private double tcth;
    
    @Getter
    @Setter
    private double bcth;

    @Getter
    @Setter
    private double strength;

    @Getter
    @Setter
    private int layer;

    @Getter
    @Setter
    private String fiberType;
    
    @Getter
    @Setter
    private String quality;
    
    public String getTensionMember(){
        return fiberType + " " + strength + "/" + layer;
    }
    
    public void createName(){
        setName(width + " " + getTensionMember() + " " + tcth + ":" + bcth + " " + quality);
    }
}
