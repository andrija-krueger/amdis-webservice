/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.entities;

import com.amdisservices.amdiswebservice.enums.OfferState;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrija Krüger
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "Offer.findAll",
            query = "SELECT o FROM Offer o"),
    @NamedQuery(name = "Offer.findByName",
            query = "SELECT o FROM Offer o WHERE o.name = :name"),
    @NamedQuery(name = "Offer.findByOfferNUmber",
            query = "SELECT o FROM Offer o WHERE o.offerNumber = :offerNumber"),})
public class Offer extends BaseEntity {

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private OfferState offerState;

    @Getter
    @Setter
    private String offerNumber;

    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Calendar finished;

    @Setter
    @OneToMany(cascade = CascadeType.ALL)
    private List<Product> products;

    public List<Product> getProducts() {
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }
}
