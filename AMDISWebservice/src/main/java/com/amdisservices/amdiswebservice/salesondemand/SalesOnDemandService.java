package com.amdisservices.amdiswebservice.salesondemand;

import com.amdisservices.amdiswebservice.dtos.UpdateInformationDto;
import com.amdisservices.amdiswebservice.entities.Offer;
import com.amdisservices.amdiswebservice.entities.Product;
import com.amdisservices.amdiswebservice.enums.OfferState;
import com.amdisservices.amdiswebservice.interfaces.ABaseService;
import com.amdisservices.amdiswebservice.interfaces.IEntity;
import com.amdisservices.amdiswebservice.interfaces.IService;
import javax.ws.rs.core.Response;

/**
 *
 * @author andri
 */
public class SalesOnDemandService extends ABaseService implements IService{

    @Override
    public Response getProduct(String crmProductId) {
         return Response.status(Response.Status.OK).entity("SalesForceService, Product was found id:  " + crmProductId).build();
    }

    @Override
    public Response updateOffer(String offerNumber) {
       return Response.status(Response.Status.OK).entity("SalesOnDemandService, a new Product was added to the Offer: " + offerNumber ).build();
    }

    @Override
    public Response createProductInCRM(Product product) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

}
