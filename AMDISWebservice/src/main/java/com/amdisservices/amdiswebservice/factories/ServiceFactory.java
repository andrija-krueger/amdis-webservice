/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.factories;

import com.amdisservices.amdiswebservice.interfaces.IService;
import com.amdisservices.amdiswebservice.salesforce.SalesForceService;
import com.amdisservices.amdiswebservice.salesondemand.SalesOnDemandService;


/**
 *
 * @author andri
 */
public class ServiceFactory {
    
    public IService getService(String service){
        IService concreteService = null;
        try {
            switch(service){
                case "SF":
                   concreteService= new SalesForceService();break;
                case "C4C":
                    concreteService = new SalesOnDemandService();break;
            }
        } catch (Exception e) {
            return null;
        }
        return concreteService;
    }
}
