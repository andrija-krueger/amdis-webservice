/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.resources;

import com.amdisservices.amdiswebservice.factories.ServiceFactory;
import com.amdisservices.amdiswebservice.interfaces.IService;
import javax.ws.rs.core.Response;

/**
 *
 * @author Andrija Krüger
 */
public abstract class ABaseResource {

    IService concreteService;

    public void getConcreteService(String service) {
        concreteService = new ServiceFactory().getService(service);
    }
    
    public Response noServiceFound(String service) {
         return Response.status(Response.Status.NOT_FOUND).entity(service + " is not available.").build();
    }
}
