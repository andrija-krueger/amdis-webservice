/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Root Resource to update an offer, retun a product id
 *
 * @author Andrija Krüger
 */
@Path("/productresource")
public class ProductResource extends ABaseResource {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/updateoffer/{service}/{offernumber}")
    public Response updateOfferWithProduct(@PathParam("service") String service, @PathParam("offernumber") String offerNumber) {
        getConcreteService(service);
        if(concreteService == null) return noServiceFound(service);
        return concreteService.updateOffer(offerNumber);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getproduct/{service}/{crmproductid}")
    public Response getProductByCRMId(@PathParam("service") String service, @PathParam("crmproductid") String crmProductId) {
        getConcreteService(service);
        if(concreteService == null) return noServiceFound(service);
        return concreteService.getProduct(crmProductId);
    }
}
