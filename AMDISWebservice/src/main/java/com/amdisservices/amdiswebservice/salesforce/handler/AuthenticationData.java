/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.salesforce.handler;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrija Krüger
 */
public class AuthenticationData {
    
    @Getter
    @Setter
    private String URL;
    
    @Getter
    @Setter
    private String UserName;
    
    @Getter
    @Setter
    private String Password;
    
    @Getter
    @Setter
    private String SecurityToken;
}
