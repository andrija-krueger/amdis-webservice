/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.salesforce.handler;

import com.amdisservices.amdiswebservice.entities.Product;
import com.sforce.soap.enterprise.sobject.Product__c;

/**
 *
 * @author Andrija Krüger
 */
public class SalesForceObjMapper {

    public Product__c getMappedSProduct(Product product) {
        Product__c sProduct = new Product__c();
        sProduct.setName__c(product.getName());
        sProduct.setWidth__c(product.getWidth());
        sProduct.setTop_cover_Thickness__c(product.getTcth());
        sProduct.setBottom_cover_thickness__c(product.getBcth());
        sProduct.setStrength__c(product.getStrength());
        sProduct.setFiber_type__c(product.getFiberType());
        sProduct.setQuality__c(product.getQuality());
        return sProduct;
    }
}
