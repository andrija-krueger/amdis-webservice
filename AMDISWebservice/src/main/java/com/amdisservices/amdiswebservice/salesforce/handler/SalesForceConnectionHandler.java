/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.salesforce.handler;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.ws.ConnectorConfig;
import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Andrija Krüger
 */
public class SalesForceConnectionHandler {

    static AuthenticationData data;

    public static EnterpriseConnection getConnection() {

        try {
            data = getAuthenticationData();
            ConnectorConfig config = new ConnectorConfig();
            config.setUsername(data.getUserName());
            config.setPassword(data.getPassword()+data.getSecurityToken());
            config.setAuthEndpoint(data.getURL());

            return new EnterpriseConnection(config);
        } catch (Exception ex) {
            System.out.println("com.amdisservices.amdiswebservice.salesforce.SalesForceConnectionHandler.getConnection()");
        }
        return null;
    }

    private static AuthenticationData getAuthenticationData() {
        AuthenticationData dauthData = new AuthenticationData();
        try {
            JSONParser parser = new JSONParser();
            JSONArray array = (JSONArray) parser.parse(new FileReader("D:\\Projekte\\AMDIS Webservice\\AMDISWebservice\\src\\main\\java\\com\\amdisservices\\amdiswebservice\\salesforce\\SFConfig.json"));

            if (array != null && array.size() > 0) {
                for (Object item : array) {
                    JSONObject dataItem = (JSONObject) item;
                    dauthData.setURL((String) dataItem.get("URL"));
                    dauthData.setUserName((String) dataItem.get("UserName"));
                    dauthData.setPassword((String) dataItem.get("Password"));
                    dauthData.setSecurityToken((String) dataItem.get("SecurityToken"));
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("com.amdisservices.amdiswebservice.salesforce.SalesForceConnectionHandler.getConnection()");
        }
        return dauthData;
    }

}
