/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.salesforce.handler;

import com.amdisservices.amdiswebservice.dtos.UpdateInformationDto;
import com.amdisservices.amdiswebservice.entities.Product;
import com.amdisservices.amdiswebservice.enums.MessageType;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.sobject.Product__c;
import com.sforce.soap.enterprise.sobject.SObject;
import javax.ws.rs.core.Response;

/**
 * Main Product Handler CRUD operations
 * @author Andrija Krüger
 * 
 */
public class SFWebserviceProductHandler {

    EnterpriseConnection connection;
    SalesForceObjMapper objMapper;
    
    public SFWebserviceProductHandler(EnterpriseConnection connection) {
        this.connection = connection;
        objMapper = new SalesForceObjMapper();
    }
     
    public UpdateInformationDto createProductInService(Product product) {
        UpdateInformationDto messageDto = new UpdateInformationDto();
        try {
            Product__c sProduct =  objMapper.getMappedSProduct(product);
            SaveResult[] result = connection.create(new SObject[]{sProduct});

            if (result.length > 0) {
                SaveResult sResult = result[0];
                if (sResult.getSuccess()) {
                    messageDto.setProductAddedToService(true);
                    messageDto.setServiceProductId(sResult.getId());
                    messageDto.addNewMessage(MessageType.info, "Product was created in salesforce");
                } else {
                    for (com.sforce.soap.enterprise.Error error : sResult.getErrors()) {
                        messageDto.addNewMessage(MessageType.error, error.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            messageDto.setProductAddedToOffer(false);
            messageDto.addNewMessage(MessageType.error, "Failed to add a new product to salesforce");
        }
        return messageDto;
    }

    public Response getProductFromService(String crmProductId) {
       // Product__c sProduct = connection.query(queryString)
       return Response.status(Response.Status.BAD_REQUEST).entity("Not impleneted").build();
    }
}
