/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.salesforce;

import com.amdisservices.amdiswebservice.salesforce.handler.SalesForceConnectionHandler;
import com.amdisservices.amdiswebservice.dtos.UpdateInformationDto;
import com.amdisservices.amdiswebservice.entities.Offer;
import com.amdisservices.amdiswebservice.entities.Product;
import com.amdisservices.amdiswebservice.enums.OfferState;
import com.amdisservices.amdiswebservice.enums.RepositoryAction;
import com.amdisservices.amdiswebservice.interfaces.ABaseService;
import com.amdisservices.amdiswebservice.interfaces.IService;
import com.amdisservices.amdiswebservice.json.DB;
import com.amdisservices.amdiswebservice.repositories.MockProductRepository;
import com.amdisservices.amdiswebservice.salesforce.handler.SFWebserviceProductHandler;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.sobject.Offer_Position__c;
import com.sforce.soap.enterprise.sobject.Offer__c;
import com.sforce.soap.enterprise.sobject.Product__c;
import com.sforce.soap.enterprise.sobject.SObject;
import java.util.List;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

/**
 *
 * @author Andrija Krüger
 */
public class SalesForceService extends ABaseService implements IService {

    EnterpriseConnection connection;
    SFWebserviceProductHandler productHandler;

    public SalesForceService() {
        connection = SalesForceConnectionHandler.getConnection();
        productHandler = new SFWebserviceProductHandler(connection);
    }

    @Override
    public Response getProduct(String crmProductId) {
        return  productHandler.getProductFromService(crmProductId);
    }

    @Override
    public Response updateOffer(String offerNumber) {
        try {
            if (connection == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("No connection to salesforceendpoint").build();
            }
            
            Offer offer = getPersistOffer(offerNumber);
            Product product = MockProductRepository.getInstance().getProduct();

            Offer offerPersist = (Offer)baseRespository.getByOfferNumber(offerNumber);
            
            
            baseRespository.update(product, RepositoryAction.update);
            
            UpdateInformationDto messages = productHandler.createProductInService(product);

            if (messages.isProductAddedToService()) {
                product.setSalesforceId(messages.getServiceProductId());
                offer.getProducts().add(product);
            }
            
            UpdateInformationDto messagess = addProductToOffer(product, offerNumber);
            

            GenericEntity<List<Offer>> list = new GenericEntity<List<Offer>>(db.getOffers()) {
            };
            //if (message.getType() == MessageType.error) {
            //  return Response.status(Response.Status.CONFLICT).entity("The obj was not savend. MEssage: " + message.getMessage()).build();
            //}
            return Response.status(Response.Status.OK).entity(list).build();
        } catch (Exception e) {
            System.out.println("com.amdisservices.amdiswebservice.salesforce.SalesForceService.updateOffer()");
        }

        return Response.status(Response.Status.BAD_REQUEST).entity("No response data").build();
    }

   

    private UpdateInformationDto addProductToOffer(Product product, String offerNumber) {
        UpdateInformationDto messageDto = new UpdateInformationDto();
        try {
            
            String queryOffer = "Select Id, Name from Offer__c WHERE Name = '" + offerNumber +"'";
            QueryResult offerResult = connection.query(queryOffer);
            
            Offer__c persistOffer = (Offer__c)offerResult.getRecords()[0];
            
            Offer_Position__c position = new Offer_Position__c();
            position.setProduct__c(product.getSalesforceId());
            position.setLength__c(400.0);
            position.setOffer__c(persistOffer.getId());
            
            SaveResult[] result = connection.create(new SObject[]{position});
            if(result.length > 0){
                 SaveResult sResult = result[0];
                 if(sResult.getSuccess()){
                     
                 }
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return messageDto;
    }

    @Override
    public Response createProductInCRM(Product product) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
