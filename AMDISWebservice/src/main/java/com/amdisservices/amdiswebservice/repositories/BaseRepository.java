/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.repositories;

import com.amdisservices.amdiswebservice.dtos.MessageDto;
import com.amdisservices.amdiswebservice.entities.Offer;
import com.amdisservices.amdiswebservice.enums.MessageType;
import com.amdisservices.amdiswebservice.enums.RepositoryAction;
import com.amdisservices.amdiswebservice.interfaces.IBaseRespository;
import com.amdisservices.amdiswebservice.interfaces.IEntity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author Andrija Krüger
 */
public class BaseRepository implements IBaseRespository {

    EntityManagerFactory emf;
    EntityManager em;

    @Override
    public Object getById(Class type, int id) {
        init();
        return em.find(type, id);
    }

    private void init() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        em = emf.createEntityManager();
    }

    @Override
    public MessageDto update(IEntity entity, RepositoryAction action) {
        init();

        if (entity.getId() == 0) {
            entity.setCreated();
        }
        entity.setModified();

        MessageDto dto = new MessageDto(MessageType.info, "init Message in Repository");
        em.getTransaction().begin();
        try {
            switch (action) {
                case update:
                    dto.setType(MessageType.info);
                    dto.setMessage("Entity updated");
                    em.persist(entity);
                    break;
                case remove:
                    dto.setType(MessageType.info);
                    dto.setMessage("Entity removed");
                    em.remove(em.find(entity.getClass(), entity.getId()));
                    break;
            }

            em.getTransaction().commit();

            em.close();
            emf.close();
        } catch (Exception e) {
            dto.setType(MessageType.error);
            dto.setMessage("Failed to " + action + " entity");
            em.getTransaction().rollback();
        }

        return dto;
    }

    @Override
    public Offer getByOfferNumber(String offerNumber) {
        TypedQuery<Offer> query = em.createNamedQuery("Offer.findAll", Offer.class);
        List<Offer> results = query.getResultList();

        return results.get(0);
    }

}
