/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.repositories;

import com.amdisservices.amdiswebservice.entities.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import lombok.Getter;

/**
 *
 * @author Andrija Krüger
 */
public class MockProductRepository extends BaseRepository{
    
    Random random;
    @Getter
    List<Product> products;
    static MockProductRepository mockProductRepository;

    private MockProductRepository() {
        random = new Random();
        products = new ArrayList<>();
        products.add(createNewProduct(0, 650, 5, 2, "EP", 500, 1, "A"));
        products.add(createNewProduct(0, 850, 6, 3, "EP", 400, 1, "AA"));
        products.add(createNewProduct(0, 950, 2, 6, "EE", 500, 2, "M"));
        products.add(createNewProduct(0, 1250, 3, 2, "E", 700, 2, "Y"));
        products.add(createNewProduct(0, 1100, 6, 3, "E", 800, 3, "Z"));
        products.add(createNewProduct(0, 700, 7, 5, "EP", 800, 5, "S"));
        products.add(createNewProduct(0, 860, 4, 7, "EE", 400, 2, "AA"));
        products.add(createNewProduct(0, 980, 3, 2, "E", 700, 3, "AA-M"));
        products.add(createNewProduct(0, 1320, 2,3, "EP", 900, 7, "A"));
    }
    
    public static MockProductRepository getInstance(){
        if(mockProductRepository == null) mockProductRepository = new MockProductRepository();
        return mockProductRepository;
    }
    
    public Product getProduct(){
        int index = random.nextInt(8);
        return products.get(index);
    }
    
    
    private Product createNewProduct(int id, int width, int tc, int bc, String fiber, int strength, int layer, String quality){
        Product p = new Product();
        p.setId(id);
        p.setWidth(width);
        p.setBcth(bc);
        p.setFiberType(fiber);
        p.setLayer(layer);
        p.setQuality(quality);
        p.setStrength(strength);
        p.setTcth(tc);
        p.createName();
        return p;
    }
    
}
