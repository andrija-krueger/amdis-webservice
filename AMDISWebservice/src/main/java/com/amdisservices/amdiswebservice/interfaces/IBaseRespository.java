/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.interfaces;

import com.amdisservices.amdiswebservice.dtos.MessageDto;
import com.amdisservices.amdiswebservice.entities.Offer;
import com.amdisservices.amdiswebservice.enums.RepositoryAction;


/**
 *
 * @author Andrija Krüger
 */
public interface IBaseRespository<T> {
    MessageDto update(IEntity entity, RepositoryAction action);
    T getById(Class<T> type, int id);
    Offer getByOfferNumber(String offerNumber);
}
