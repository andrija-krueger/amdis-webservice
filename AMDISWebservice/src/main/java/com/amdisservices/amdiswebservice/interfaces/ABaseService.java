/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.interfaces;

import com.amdisservices.amdiswebservice.entities.Offer;
import com.amdisservices.amdiswebservice.enums.OfferState;
import com.amdisservices.amdiswebservice.json.DB;
import com.amdisservices.amdiswebservice.repositories.BaseRepository;
import java.util.Random;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrija Krüger
 */
public abstract class ABaseService {

    public IBaseRespository baseRespository;
    public Random random = new Random();
    public DB db;

    public ABaseService() {
        if (baseRespository == null) {
            baseRespository = new BaseRepository();
        }
        db = DB.getInstance();
    }

    public Offer getPersistOffer(String offerNumber) {
        Offer offer = new Offer();
        for (Offer persistOffer : db.getOffers()) {
            if (offer.getOfferNumber() != null && offer.getOfferNumber().equals(offerNumber)) {
                offer = persistOffer;
                break;
            }
        }
        
        offer.setOfferNumber(offerNumber);
        offer.setOfferState(OfferState.created);
        
        if (offer.getId() == 0) {
            offer.setId(random.nextInt(500));
            db.getOffers().add(offer);
        };

        return offer;

    }
}
