/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.interfaces;

/**
 *
 * @author Andrija Krüger
 */
public interface IOfferState {
    IOfferState nextState();
    IOfferState backState();
    void operate();// test
}
