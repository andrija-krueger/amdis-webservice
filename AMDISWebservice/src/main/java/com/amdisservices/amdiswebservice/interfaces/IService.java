/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.interfaces;

import com.amdisservices.amdiswebservice.entities.Product;
import javax.ws.rs.core.Response;

/**
 *
 * @author Andrija Krüger
 */
public interface IService {
    Response getProduct(String crmProductId);
    Response updateOffer(String offerNumber);
    Response createProductInCRM(Product product);
}
