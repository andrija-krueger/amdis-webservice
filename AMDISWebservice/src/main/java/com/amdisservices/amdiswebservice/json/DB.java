/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.json;

import com.amdisservices.amdiswebservice.entities.Offer;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author Andrija Krüger
 */
public class DB {
    
    @Getter
    List<Offer> offers;
    static DB db;
    
    private DB(){
        offers = new ArrayList<>();
    }
    
    public static DB getInstance(){
        if(db == null) db = new DB();
        return db;
    }
    
}
