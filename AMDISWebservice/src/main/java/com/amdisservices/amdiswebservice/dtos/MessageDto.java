/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.dtos;

import com.amdisservices.amdiswebservice.enums.MessageType;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * Message object to return errors or information messages
 *
 * @author Andrija Krüger
 */
public class MessageDto {

    @Getter
    @Setter
    private MessageType type;
    @Getter
    @Setter
    private String message;

    public MessageDto(MessageType type, String messageText) {
        this.type = type;
        this.message = messageText;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.type);
        hash = 67 * hash + Objects.hashCode(this.message);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessageDto other = (MessageDto) obj;
        if (!Objects.equals(this.message, other.message)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MessageDto{" + "type=" + type + ", message=" + message + '}';
    }
}
