/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amdisservices.amdiswebservice.dtos;

import com.amdisservices.amdiswebservice.enums.MessageType;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrija Krüger
 */
public class UpdateInformationDto {

    @Getter
    @Setter
    private int offerNumber;
    @Getter
    @Setter
    private String serviceProductId;
    @Getter
    @Setter
    private boolean productAddedToOffer;
    @Getter
    @Setter
    private boolean productAddedToService;
    @Getter
    @Setter
    private List<MessageDto> messages;
    
    public void addNewMessage(MessageType type, String messageText){
        if(messages == null) messages = new ArrayList<>();
        try {
             messages.add(new MessageDto(type, messageText));
        } catch (Exception e) {
             messages.add(new MessageDto(MessageType.error, "Failed to create a new Message and adding to the message list."));
        }
    }
}
